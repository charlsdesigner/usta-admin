import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImageCropperModule } from 'ngx-image-cropper';

// Firebase

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';

// Material
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule
} from '@angular/material';

// Servicios

import { AuthService } from './services/auth.service';
import { MainService } from './services/main.service';
import { UploadService } from './services/upload.service';
import { TestimonialsService } from './services/testimonials.service';
import { PushmessageService } from './services/pushmessage.service';
import { MyGuardService } from './services/my-guard.service';

import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { CreateComponent } from './create/create.component';
import { AddClaseDialog } from './modals/add-clase.component';
import { CoursesComponent } from './courses/courses.component';
import { EditComponent } from './edit/edit.component';
import { UsersComponent } from './users/users.component';
import { WellcomeComponent } from './wellcome/wellcome.component';
import { FiltersPipe } from './pipes/filters.pipe';
import { TypesComponent } from './types/types.component';
import { UploadPicsComponent } from './upload-pics/upload-pics.component';
import { UploadFilesComponent } from './upload-files/upload-files.component';
import { AreasComponent } from './areas/areas.component';
import { ClasesComponent } from './clases/clases.component';
import { TypesListComponent } from './types-list/types-list.component';
import { AreasListComponent } from './areas-list/areas-list.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { PushmessageComponent } from './pushmessage/pushmessage.component';



// Routes
const appRoutes: Routes = [
  { path: '', component: DashboardComponent, canActivate: [MyGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [MyGuardService] },
  { path: 'create', component: CreateComponent, canActivate: [MyGuardService] },
  { path: 'types', component: TypesComponent, canActivate: [MyGuardService] },
  { path: 'types-list', component: TypesListComponent, canActivate: [MyGuardService] },
  { path: 'areas', component: AreasComponent, canActivate: [MyGuardService]},
  { path: 'areas-list', component: AreasListComponent, canActivate: [MyGuardService] },
  { path: 'categrories', component: ClasesComponent, canActivate: [MyGuardService]},
  { path: 'courses', component: CoursesComponent, canActivate: [MyGuardService] },
  { path: 'edit/:type/:area/:course', component: EditComponent, canActivate: [MyGuardService] },
  { path: 'users', component: UsersComponent, canActivate: [MyGuardService] },
  { path: 'testimonios', component: TestimonialsComponent, canActivate: [MyGuardService] },
  { path: 'pushmessages', component: PushmessageComponent, canActivate: [MyGuardService] },
];

// Firebase Config
export const firebaseConfig = {
  apiKey: 'AIzaSyCnzHBuJ-RPEmiBx40JG5rsTx6VT4iiUW0',
  authDomain: 'usantoto-693aa.firebaseapp.com',
  databaseURL: 'https://usantoto-693aa.firebaseio.com',
  projectId: 'usantoto-693aa',
  storageBucket: 'usantoto-693aa.appspot.com',
  messagingSenderId: '1007761166932'
};


@NgModule({
  declarations: [
    AddClaseDialog,
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    LoginComponent,
    CreateComponent,
    CoursesComponent,
    EditComponent,
    UsersComponent,
    WellcomeComponent,
    FiltersPipe,
    TypesComponent,
    UploadPicsComponent,
    UploadFilesComponent,
    AreasComponent,
    ClasesComponent,
    TypesListComponent,
    AreasListComponent,
    TestimonialsComponent,
    PushmessageComponent
  ],
  imports: [
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ImageCropperModule,
    ReactiveFormsModule,
    MatCardModule,
    MatDialogModule,
    MatExpansionModule,
    MatToolbarModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatListModule,
    MatSnackBarModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatTableModule,
    MatTabsModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  entryComponents: [ AddClaseDialog ],
  providers: [AuthService, MainService, UploadService, TestimonialsService, MyGuardService, PushmessageService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
