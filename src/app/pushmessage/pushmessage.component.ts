import { Component, OnInit } from '@angular/core';
import { PushmessageService } from '../services/pushmessage.service';

@Component({
  selector: 'app-pushmessage',
  templateUrl: './pushmessage.component.html',
  styleUrls: ['./pushmessage.component.css']
})
export class PushmessageComponent implements OnInit {

  messages: any;

  title: string = '';
  content: string = '';
  action: string = '';

  constructor(private pushmessageService: PushmessageService) { }

  ngOnInit() {
    this.pushmessageService.getMessages().valueChanges().subscribe( messages => {
      this.messages = messages;
    });
  }

  sendMessage() {
    const message = {
      id: Date.now(),
      title: this.title,
      content: this.content,
      action: this.action
    };
    this.pushmessageService.sendMessage(message);
  }

}
