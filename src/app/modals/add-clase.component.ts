import { Component, OnInit } from '@angular/core';
import { MainService } from '../services/main.service';
import { Class } from '../interfaces/create';

@Component({
  selector: 'add-clase-dialog',
  templateUrl: 'add-clase.html',
})
export class AddClaseDialog {
  id = Date.now();
  clase: string;

  constructor(private mainService: MainService) {
  }
  addClass() {
    const clase: Class = {
      id: this.id,
      name: this.clase
    };
    this.mainService.createClass(clase);
  }
}
