import { Component, OnInit } from '@angular/core';
import { MainService } from '../services/main.service';
import { Course, Type, Area, Class } from '../interfaces/create';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  courses: any = [];
  tipos: Type[] = [];
  areas: Area[] = [];
  
  tipo: any;
  area: any;


  constructor(private mainService: MainService) {
    mainService.getTipos().valueChanges().subscribe( (tipos: Type[]) => {
      this.tipos = tipos;
    });
  }

  changeType(type) {
    this.tipo = type;
    this.mainService.getAreas(type).valueChanges().subscribe((areas: Area[]) => {
      this.areas = areas;
    });
  }

  changeArea(area) {
    this.area = area;
    this.mainService.getCourses(this.tipo, this.area).valueChanges().subscribe(courses => {
      this.courses = courses;
    });
  }

  ngOnInit() {
  }

}
