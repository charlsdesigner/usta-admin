import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddClaseDialog } from '../modals/add-clase.component';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { Item, Area, Type, Class, Course } from '../interfaces/create';

// Servicio
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  color = 'primary';
  checked = true;
  disabled = false;
  active: boolean = true;

  rForm: FormGroup;
  post: any;
  area: boolean = false;
  clase: boolean = false;

  uploadPic: boolean = false;
  viewpics: boolean = false;
  pisclist: any = [];

  fileList: any = [];
  viewfiles: boolean = false;
  uploadfile: boolean = false;

  content: Item[] = [];
  areas: Area[] = [];
  tipos: Type[] = [];
  clases: Class[] = [];

  fileUrl: string = '';
  selectedFile: any = [];
  pictureUrl: string = '';

  addType: boolean = false;
  addArea: boolean = false;

  constructor(private fb: FormBuilder,
              private mainService: MainService,
              public dialog: MatDialog,
              public snackBar: MatSnackBar) {
    this.rForm = fb.group({
      'name': [null, Validators.compose([
        Validators.required
      ])],
      'snies': [null, Validators.compose([
        Validators.required
      ])],
      'registro': [null, Validators.compose([
        Validators.required
      ])],
      'acreditacion': [null],
      'ubicacion': [null, Validators.compose([
        Validators.required
      ])],
      'inversion': [null, Validators.compose([
        Validators.required
      ])],
      'titulo': [null, Validators.compose([
        Validators.required
      ])],
      'area': [null, Validators.compose([
        Validators.required
      ])],
      'tipo': [null, Validators.compose([
        Validators.required
      ])],
      'clase': [null],
      'modalidad': [null, Validators.compose([
        Validators.required
      ])],
      'duracion': [null, Validators.compose([
        Validators.required
      ])],
      'jornada': [null, Validators.compose([
        Validators.required
      ])],
      'url': [null, Validators.compose([
        Validators.required,
        Validators.pattern('[a-z0-9._+-]{1,55}')
      ])],
      'video': [null, Validators.compose([
        Validators.pattern('[a-zA-Z0-9._+-]{1,25}')
      ])],
    });
   }

  changeType(type) {
    this.area = true;
    this.mainService.getAreas(type).valueChanges().subscribe((areas: Area[]) => {
      this.areas = areas;
    });
    if (type = 'posgrado') {
      this.clase = true;
    } else {
      this.clase = false;
    }
  }

  openDialog() {
    this.dialog.open(AddClaseDialog);
  }

  addItem(title, content) {
    const item: Item = {
      title: title,
      content: content
    };
    this.content.push(item);
  }

  removeItem(item) {
    const index = this.content.indexOf(item);
    this.content.splice(index, 1);
  }

  createCourse(data) {
    data.active = this.active;
    data.content = this.content;
    data.picture = this.pictureUrl;
    data.file = this.fileUrl;
    data.new = true;
    const course: Course[] = data;
    this.mainService.saveCourse(course);
  }
  selectPic(pic) {
    this.pictureUrl = pic;
    this.viewpics = false;
  }
  selectFile(file) {
    this.fileUrl = file;
    this.selectedFile = file;
    this.viewfiles = false;
  }
  removepic() {
    this.pictureUrl = '';
  }
  OpenUploader() {
    this.uploadPic =  !this.uploadPic;
  }

  OpenUploadFile() {
    this.uploadfile = !this.uploadfile;
  }

  OpenType() {
    this.addType = !this.addType;
  }
  OpenArea() {
    this.addArea = !this.addArea;
  }
  viewPics() {
    this.viewpics = !this.viewpics;
  }

  viewFiles() {
    this.viewfiles = !this.viewfiles;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  ngOnInit() {
    this.mainService.getTipos().valueChanges().subscribe((tipos: Type[]) => {
      this.tipos = tipos;
    });

    this.mainService.getClass().valueChanges().subscribe((clases: Class[]) => {
      this.clases = clases;
    });

    this.mainService.getPicList().valueChanges().subscribe( pics => {
      this.pisclist = pics;
    });

    this.mainService.getFileList().valueChanges().subscribe( files => {
      this.fileList = files;
    });
  }

}
