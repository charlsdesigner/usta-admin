import { Component, OnInit } from '@angular/core';
import { TestimonialsService } from '../services/testimonials.service';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {
  color = 'primary';
  checked = true;
  disabled = false;

  testis: any = [];
  picturesList: any = [];
  uploadPic: boolean = false;
  viewPisc: boolean = false;
  selectedPic: string = '';

  tipos: any = [];
  areas: any = [];
  cursos: any = [];

  tipo: string = '';
  area: string = '';
  curso: string = '';


  testimonio: any = {};

  constructor(private testimonialsService: TestimonialsService, private mainService: MainService) { }

  ngOnInit() {
    this.testimonialsService.getTesti().valueChanges().subscribe( testis => {
      this.testis = testis;
    });

    this.mainService.getPicList().valueChanges().subscribe( pics => {
      this.picturesList = pics;
    });

    this.mainService.getTipos().valueChanges().subscribe( tipos => {
      this.tipos = tipos;
    });
  }

  OpenUploader() {
    this.uploadPic = !this.uploadPic;
  }
  OpenGalery() {
    this.viewPisc = !this.viewPisc;
  }
  selectPic(pic) {
    this.testimonio.pic = pic;
  }

  changeType(tipo) {
    this.testimonio.tipo = tipo;
    this.getAreas();
  }
  changeArea(area) {
    this.testimonio.area = area;
    this.getCourses();
  }

  getAreas() {
    this.mainService.getAreas(this.testimonio.tipo).valueChanges().subscribe(areas => {
      this.areas = areas;
    });
  }

  getCourses() {
    this.mainService.getCourses(this.testimonio.tipo, this.testimonio.area).valueChanges().subscribe(cursos => {
      this.cursos = cursos;
    });
  }

  crearTestimonio() {
    if (!this.testimonio.id) {
      this.testimonio.id = Date.now();
    }
    this.testimonialsService.createTesti(this.testimonio);
  }

  selectTesti(testi) {
    this.testimonio = testi;
    this.getAreas();
    this.getCourses();
  }

}
