import { Component, OnInit } from '@angular/core';
import { Type } from '../interfaces/create';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-types',
  templateUrl: './types.component.html',
  styleUrls: ['./types.component.scss']
})
export class TypesComponent implements OnInit {

  tipos: Type[] = [];
  name: string;
  color: string;

  uploadPic: boolean = false;
  viewpics: boolean = false;
  pisclist: any = [];
  pictureUrl: string = '';

  constructor(private mainService: MainService) {
  }

  addTipo() {
    const id = this.name.replace(/\s+/g, '-').toLowerCase();
    const tipo: Type = {
      id: id,
      name: this.name,
      color: this.color,
      img: this.pictureUrl,
    };
    this.mainService.createTipo(tipo);
  }

  OpenUploader() {
    this.uploadPic = !this.uploadPic;
  }
  viewPics() {
    this.viewpics = !this.viewpics;
  }
  removepic() {
    this.pictureUrl = '';
  }
  selectPic(pic) {
    this.pictureUrl = pic;
    this.viewpics = false;
  }

  ngOnInit() {
    this.mainService.getPicList().valueChanges().subscribe(pics => {
      this.pisclist = pics;
    });
  }

}
