import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AddClaseDialog } from '../modals/add-clase.component';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { MainService } from '../services/main.service';
import { Item, Type, Area, Class } from '../interfaces/create';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  color = 'primary';
  checked = true;
  disabled = false;

  id: any;
  tipo: any;
  area: any;
  areas: Area[] = [];
  clases: Class[] = [];
  tipos: any = [];
  course: any = [];
  content: any = [];

  addType: boolean = false;
  addArea: boolean = false;
  pisclist: any = [];
  viewpics: boolean = false;
  uploadPic: boolean = false;

  fileList: any = [];
  viewfiles: boolean = false;
  uploadfile: boolean = false;


  constructor(private mainService: MainService,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              public snackBar: MatSnackBar) {

    this.tipo = this.route.snapshot.params['type'];
    this.area = this.route.snapshot.params['area'];
    this.id = this.route.snapshot.params['course'];
  }

  ngOnInit() {
    this.mainService.getCourse(this.tipo, this.area, this.id).valueChanges().subscribe(course => {
      this.course = course;
      this.content = this.course.content;
    });

    this.mainService.getTipos().valueChanges().subscribe((tipos: Type[]) => {
      this.tipos = tipos;
    });

    this.mainService.getAreas(this.tipo).valueChanges().subscribe((areas: Area[]) => {
      this.areas = areas;
    });

    this.mainService.getClass().valueChanges().subscribe((clases: Class[]) => {
      this.clases = clases;
    });

    this.mainService.getPicList().valueChanges().subscribe(pics => {
      this.pisclist = pics;
    });

    this.mainService.getFileList().valueChanges().subscribe(files => {
      this.fileList = files;
    });
  }

  addItem(title, content) {
    const item: Item = {
      title: title,
      content: content
    };
    if (this.content) {
      this.content.push(item);
    } else {
      this.content = [];
      this.content.push(item);
    }
  }

  removeItem(item) {
    const index = this.content.indexOf(item);
    this.content.splice(index, 1);
  }

  updateCourse() {
    this.course.content = this.content;
    this.course.new = false;
    this.mainService.editCourse(this.course);
  }

  changeType(type) {
    this.area = true;
    this.mainService.getAreas(type).valueChanges().subscribe((areas: Area[]) => {
      this.areas = areas;
    });
  }

  viewPics() {
    this.viewpics = !this.viewpics;
  }
  removepic() {
    this.course.picture = '';
  }
  OpenType() {
    this.addType = !this.addType;
  }
  OpenArea() {
    this.addArea = !this.addArea;
  }
  OpenUploader() {
    this.uploadPic = !this.uploadPic;
  }
  selectPic(pic) {
    this.course.picture = pic;
    this.viewpics = false;
  }
  selectFile(file) {
    this.course.picture = file;
    this.viewfiles = false;
  }
  OpenUploadFile() {
    this.uploadfile = !this.uploadfile;
  }
  viewFiles() {
    this.viewfiles = !this.viewfiles;
  }
  openDialog() {
    this.dialog.open(AddClaseDialog);
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
