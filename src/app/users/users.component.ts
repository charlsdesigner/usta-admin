import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: any = [];
  singinParams: any = {};
  user: boolean = false;
  constructor(private authService: AuthService) {}
  singIn() {
    this.authService.singin(this.singinParams.email, this.singinParams.password, this.singinParams.name);
  }
  updateUser(user) {
    this.user = true;
    this.singinParams = user;
  }
  update() {
    this.authService.updateUser(this.singinParams );
  }
  ngOnInit() {
    this.authService.getUsers().valueChanges().subscribe(users => {
      this.users = users;
    });
  }

}
