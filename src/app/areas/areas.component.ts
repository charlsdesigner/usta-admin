import { Component, OnInit } from '@angular/core';
import { Area, Type } from '../interfaces/create';
// Servicio
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss']
})
export class AreasComponent implements OnInit {

  name: string;
  tipos: Type[] = [];
  tipo: string;

  uploadPic: boolean = false;
  viewpics: boolean = false;
  viewicons: boolean = false;
  pisclist: any = [];
  pictureUrl: string = '';
  iconUrl: string = '';


  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.mainService.getTipos().valueChanges().subscribe((tipos: Type[]) => {
      this.tipos = tipos;
    });

    this.mainService.getPicList().valueChanges().subscribe(pics => {
      this.pisclist = pics;
    });
  }

  addArea() {
    const id = this.name.replace(/\s+/g, '-').toLowerCase();
    const area: Area = {
      id: id,
      name: this.name,
      img: this.pictureUrl,
      icono: this.iconUrl,
    };
    this.mainService.createArea(this.tipo, area);
  }

  OpenUploader() {
    this.uploadPic = !this.uploadPic;
  }
  viewPics() {
    this.viewpics = !this.viewpics;
  }
  viewIcons() {
    this.viewicons = !this.viewicons;
  }
  removepic() {
    this.pictureUrl = '';
  }
  removeicon() {
    this.iconUrl = '';
  }
  selectPic(pic) {
    this.pictureUrl = pic;
    this.viewpics = false;
  }
  selectIcon(icon) {
    this.iconUrl = icon;
    this.viewicons = false;
  }

}
