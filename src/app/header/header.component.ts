import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  loggedUser: any = null;

  constructor(private authService: AuthService) {

  }

  loggout() {
    this.authService.loggout();
  }
  ngOnInit() {
    setTimeout(() => {
      this.loggedUser = this.authService.getUser().currentUser.email;
    }, 1000);
  }

}
