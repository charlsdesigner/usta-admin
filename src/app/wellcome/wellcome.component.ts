import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-wellcome',
  templateUrl: './wellcome.component.html',
  styleUrls: ['./wellcome.component.scss']
})
export class WellcomeComponent implements OnInit {

  actionText: string = 'the app is Loadin ...';
  action: string = 'login';
  actionLabel: string = 'Go to Login';
  loggedUser: any = null;

  constructor(private authService: AuthService, private router: Router) {

  }

  ngOnInit() {
    this.authService.isLogged()
      .subscribe((result) => {
        if (result && result.uid) {
          setTimeout(() => {
            this.loggedUser = this.authService.getUser().currentUser.email;
            this.actionText = this.loggedUser + ', the app is ready to start working';
            this.action = 'dashboard';
            this.actionLabel = 'Go to dashboard';
          }, 500);
        } else {
          setTimeout(() => {
            this.actionText = 'the app is ready to start, go to login';
          }, 500);
        }
      }, (error) => {
        this.actionText = 'There is an error in this moment, check your internet conection';
      });
  }

}
