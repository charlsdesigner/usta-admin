export interface Item {
    title: string;
    content: string;
}

export interface Area {
    id: any;
    name: string;
    img?: string;
    icono?: string;
}

export interface Type {
    id: any;
    name: string;
    color: string;
    img?: string;
}

export interface Class {
    id: any;
    name: string;
}

export interface Course {
    name: string;
    acreditacion: string;
    area: Area;
    content?: Item;
    duracion: string;
    inversion: string;
    jornada: string;
    new?: boolean;
    modalidad: string;
    picture: string;
    resgistro: string;
    snies: string;
    tipo: Type;
    clase?: Class;
    hq?: boolean;
    titulo: string;
    ubicacion: string;
    url: string;
    video: string;
}
