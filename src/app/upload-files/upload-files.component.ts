import { Component, OnInit } from '@angular/core';
import { UploadService } from '../services/upload.service';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.scss']
})
export class UploadFilesComponent implements OnInit {

  constructor(private uploadService: UploadService, public snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  fileChangeEvent(event: any): void {
    if (event.target.files[0].size <= 120000) {
      this.uploadService.uploadFile(event);
      this.openSnackBar('Procesando archivo', 'Ok');
    } else {
      this.openSnackBar('El archivo exede el peso limite', event.target.files[0].size + '/KB');
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
