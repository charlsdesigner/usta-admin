import { TestBed, inject } from '@angular/core/testing';

import { PushmessageService } from './pushmessage.service';

describe('PushmessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PushmessageService]
    });
  });

  it('should be created', inject([PushmessageService], (service: PushmessageService) => {
    expect(service).toBeTruthy();
  }));
});
