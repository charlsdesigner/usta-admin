import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database/database';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class PushmessageService {

  constructor(private afDB: AngularFireDatabase, public snackBar: MatSnackBar, private router: Router) { }


  public sendMessage(message) {
    this.afDB.database.ref('data/pushmessages/' + message.id).set(message)
      .then((response) => {
        this.openSnackBar('Mesaje Enviado', response);
      })
      .catch((error) => {
        console.log(error);
        this.openSnackBar(error, 'Intentar de nuevo');
      });
  }

  public getMessages() {
    return this.afDB.list('data/pushmessages/');
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
