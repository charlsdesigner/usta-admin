import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database/database';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class TestimonialsService {

  constructor(private afDB: AngularFireDatabase, public snackBar: MatSnackBar) { }

  public createTesti(testi) {
    this.afDB.database.ref('data/testimonials/' + testi.id).set(testi)
      .then((response) => {
        this.openSnackBar('Guardado con exito', response);
      })
      .catch((error) => {
        console.log(error);
        this.openSnackBar(error, 'Intentar de nuevo');
      });
  }

  public getTesti() {
    return this.afDB.list('data/testimonials/');
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
