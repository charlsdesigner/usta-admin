import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database/database';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(private afDB: AngularFireDatabase, public snackBar: MatSnackBar, private router: Router) { }
  // Areas
  public getAreas(type) {
    return this.afDB.list('data/types/' + type + '/areas');
  }
  public createArea(type, area) {
    this.afDB.database.ref('data/types/' + type + '/areas/' + area.id).set(area)
      .then((response) => {
        this.openSnackBar('Creado correctamente', response);
      })
      .catch((error) => {
        console.log(error);
        this.openSnackBar(error, 'Intentar de nuevo');
      });
  }
  public editArea(type, area) {
    this.afDB.database.ref('data/types/' + type + '/areas/' + area.id).update(area)
      .then((response) => {
        this.openSnackBar('Editado correctamente', response);
      })
      .catch((error) => {
        console.log(error);
        this.openSnackBar(error, 'Intentar de nuevo');
      });
  }

  // Areas
  public getClass() {
    return this.afDB.list('data/clases/');
  }
  public createClass(clase) {
    this.afDB.database.ref('data/clases/' + clase.id).set(clase)
      .then((response) => {
        this.openSnackBar('Creado correctamente', response);
      })
      .catch((error) => {
        console.log(error);
        this.openSnackBar(error, 'Intentar de nuevo');
      });
  }
  public editClass(clase) {
    this.afDB.database.ref('data/clases/' + clase.id).update(clase)
      .then((response) => {
        this.openSnackBar('Editado correctamente', response);
      })
      .catch((error) => {
        console.log(error);
        this.openSnackBar(error, 'Intentar de nuevo');
      });
  }

  // Tipos
  public getTipos() {
    return this.afDB.list('data/types/');
  }
  public createTipo(tipo) {
    this.afDB.database.ref('data/types/' + tipo.id).set(tipo)
      .then((response) => {
        this.openSnackBar('Creado correctamente', response);
      })
      .catch((error) => {
        console.log(error);
        this.openSnackBar(error, 'Intentar de nuevo');
      });
  }
  public editTipo(tipo) {
    this.afDB.database.ref('data/types/' + tipo.id).update(tipo)
      .then((response) => {
        this.openSnackBar('Editado correctamente', response);
      })
      .catch((error) => {
        console.log(error);
        this.openSnackBar(error, 'Intentar de nuevo');
      });
  }

  // Cursos
  public getCourses(type, area) {
    return this.afDB.list('data/types/' + type + '/areas/' + area + '/courses/');
  }
  public saveCourse(course) {
    this.afDB.database.ref('data/types/' + course.tipo + '/areas/' + course.area + '/courses/' + course.url).set(course)
      .then((response) => {
        this.afDB.database.ref('data/allcourses/' + course.url).set(course);
        this.openSnackBar('Curso creado correctamente', response);
        setTimeout(() => {
          this.router.navigate(['edit/' + course.tipo + '/' + course.area + '/' + course.url]);
        }, 800);
      })
      .catch((error) => {
        console.log(error);
        this.openSnackBar(error, 'Intentar de nuevo');
      });
  }
  public editCourse(course) {
    this.afDB.database.ref('data/types/' + course.tipo + '/areas/' + course.area + '/courses/' + course.url).update(course)
      .then((response) => {
        this.afDB.database.ref('data/allcourses/' + course.url).update(course);
        this.openSnackBar('Curso actualizado correctamente', response);
      })
      .catch((error) => {
        console.log(error);
        this.openSnackBar(error, 'Intentar de nuevo');
      });
  }
  public getCourse(type, area, id) {
    return this.afDB.object('data/types/' + type + '/areas/' + area + '/courses/' + id);
  }

  // Media

  public getPicList() {
    return this.afDB.list('data/uploads/pictures/');
  }

  public getFileList() {
    return this.afDB.list('data/uploads/files/');
  }


  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
