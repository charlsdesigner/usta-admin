import { Component, OnInit } from '@angular/core';
import { MainService } from '../services/main.service';
import { Type } from '../interfaces/create';

@Component({
  selector: 'app-types-list',
  templateUrl: './types-list.component.html',
  styleUrls: ['./types-list.component.css']
})
export class TypesListComponent implements OnInit {

  types: Type[] = [];
  tipo: any = [];
  edit: boolean = false;

  uploadPic: boolean = false;
  viewpics: boolean = false;
  pisclist: any;

  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.mainService.getTipos().valueChanges().subscribe((tipos: Type[]) => {
      this.types = tipos;
    });
    this.mainService.getPicList().valueChanges().subscribe(pics => {
      this.pisclist = pics;
    });
  }

  editType(tipo) {
    this.tipo = tipo;
    this.edit = true;
  }
  saveType() {
    this.mainService.editTipo(this.tipo);
  }
  OpenUploader() {
    this.uploadPic = !this.uploadPic;
  }
  viewPics() {
    this.viewpics = !this.viewpics;
  }
  selectPic(pic) {
    this.tipo.img = pic;
    this.viewpics = false;
  }
}
