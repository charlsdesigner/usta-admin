import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UploadService } from '../services/upload.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-upload-pics',
  templateUrl: './upload-pics.component.html',
  styleUrls: ['./upload-pics.component.scss']
})
export class UploadPicsComponent implements OnInit {

  imageChangedEvent: any = '';
  croppedImage: any = '';
  picture: any;
  aspectRatio: any = '';
  resizeToWidth: any = '';

  constructor(private uploadService: UploadService, public snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  UploadImage() {
    this.uploadService.UploadImage(this.croppedImage);
    this.imageChangedEvent = '';
    this.croppedImage = '';
  }

  fileChangeEvent(event: any): void {
    if (event.target.files[0].size <= 1200000) {
      this.imageChangedEvent = event;
    } else {
      this.openSnackBar('El archivo exede el peso limite', event.target.files[0].size + '/KB');
    }
  }
  imageCropped(image: string) {
    this.croppedImage = image;
  }
  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    // show message
    this.openSnackBar('Hubo un error', 'Intentar de nuevo');
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
