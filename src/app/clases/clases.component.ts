import { Component, OnInit } from '@angular/core';
import { AddClaseDialog } from '../modals/add-clase.component';
import { MatDialog } from '@angular/material';
import { Class } from '../interfaces/create';
import { MainService } from '../services/main.service';


@Component({
  selector: 'app-clases',
  templateUrl: './clases.component.html',
  styleUrls: ['./clases.component.css']
})
export class ClasesComponent implements OnInit {
  clases: Class[] = [];
  categoria: any;
  edit: boolean = false;

  constructor(private mainService: MainService, public dialog: MatDialog) { }

  ngOnInit() {
    this.mainService.getClass().valueChanges().subscribe((clases: Class[]) => {
      this.clases = clases;
    });
  }

  openDialog() {
    this.dialog.open(AddClaseDialog);
  }

  editCat(clase) {
    this.edit = true;
    this.categoria = clase;
  }

  saveCat() {
    this.mainService.editClass(this.categoria);
  }

}
