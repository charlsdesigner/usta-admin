import { Component, OnInit } from '@angular/core';
import { MainService } from '../services/main.service';
import { Type, Area } from '../interfaces/create';

@Component({
  selector: 'app-areas-list',
  templateUrl: './areas-list.component.html',
  styleUrls: ['./areas-list.component.css']
})
export class AreasListComponent implements OnInit {

  types: Type[] = [];
  tipo: any = [];
  areas: Area[] = [];
  area: any = [];
  edit: boolean = false;

  uploadPic: boolean = false;
  viewpics: boolean = false;
  viewicons: boolean = false;
  pisclist: any;

  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.mainService.getTipos().valueChanges().subscribe((tipos: Type[]) => {
      this.types = tipos;
    });
    this.mainService.getPicList().valueChanges().subscribe(pics => {
      this.pisclist = pics;
    });
  }

  changeType(event) {
    this.mainService.getAreas(event).valueChanges().subscribe((areas: Area[]) => {
      this.areas = areas;
    });
  }

  editArea(area)  {
    this.area = area;
    this.edit = true;
  }
  saveArea() {
    this.mainService.editArea(this.tipo, this.area);
  }
  OpenUploader() {
    this.uploadPic = !this.uploadPic;
  }
  viewPics() {
    this.viewpics = !this.viewpics;
  }
  viewIcons() {
    this.viewicons = !this.viewicons;
  }
  selectPic(pic) {
    this.area.img = pic;
    this.viewpics = false;
  }
  selectIcon(icon) {
    this.area.icono = icon;
    this.viewicons = false;
  }

}
