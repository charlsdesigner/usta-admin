import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'admin';
  loc: boolean = true;
  constructor(private location: Location) {
    let outlet = this.location.prepareExternalUrl(this.location.path());
    if (outlet = 'login') {
      this.loc = false;
    }
  }
}
