import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  title = 'login';
  loginParams: any = {};
  loggedIn = false;
  loggedUser: any = null;

  constructor(private authService: AuthService, private router: Router) {
    this.authService.isLogged()
      .subscribe((result) => {
        if (result && result.uid) {
          setTimeout(() => {
            this.router.navigate(['dashboard']);
          }, 800);
        } else {
          this.loggedIn = false;
        }
      }, (error) => {
        this.loggedIn = false;
      });
  }

  login() {
    this.authService.login(this.loginParams.email, this.loginParams.password);
  }

  ngOnInit() {
  }

}
